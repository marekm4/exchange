import java.util.HashMap;

public class Wallet {
    private HashMap<Currency, Integer> assets = new HashMap<Currency, Integer>();

    public void addAsset(Currency currency, Integer amount) {
        this.assets.put(currency, amount);
    }

    public HashMap<Currency, Integer> getAssets() {
        return (HashMap<Currency, Integer>)this.assets.clone();
    }
}
