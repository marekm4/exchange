import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Main {

    static class Item {
        String name;
        String code;
        Integer unit;
        Double purchasePrice;
        Double sellPrice;
        Double averagePrice;
    }

    static class Response {
        Date publicationDate;
        List<Item> items;
    }

    public static void main(String[] args) {
        try {
            List<ExchangeRate> exchangeRates = getExchangeRates();

            exchangeRates.forEach((rate) -> System.out.printf("Currency: %s, Sell: %f, Purchase: %f\n", rate.getCurrency().getName(), rate.getSellPrice(), rate.getPurchasePrice()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Currency usd = new Currency("US Dollar", "USD");
        Currency euro = new Currency("Euro", "EUR");

        Wallet wallet = new Wallet();
        wallet.addAsset(usd, 15);
        wallet.addAsset(euro, 100);

        wallet.getAssets().forEach((currency, amount) -> System.out.printf("Currency: %s, Amount: %d\n", currency.getName(), amount));
    }

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    private static List<ExchangeRate> getExchangeRates() throws Exception {
		String json = readUrl("http://webtask.future-processing.com:8068/currencies");

        Gson gson = new Gson();
        Response response = gson.fromJson(json, Response.class);

        List<ExchangeRate> exchangeRates = new LinkedList<ExchangeRate>();
        response.items.forEach((item) -> {
            exchangeRates.add(new ExchangeRate(new Currency(item.name, item.code), item.purchasePrice, item.sellPrice));
        });
        return exchangeRates;
    }
}
