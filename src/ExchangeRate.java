public class ExchangeRate {
    private Currency currency;
    private double purchasePrice;
    private double sellPrice;

    public ExchangeRate(Currency currency, double purchasePrice, double sellPrice) {
        this.currency = currency;
        this.purchasePrice = purchasePrice;
        this.sellPrice = sellPrice;
    }

    public Currency getCurrency() {
        return currency;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public double getSellPrice() {
        return sellPrice;
    }
}
